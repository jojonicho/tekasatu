from django.urls import path, include
from . import views

app_name = 'checkout'
urlpatterns = [
    path('handle_transaksi/', views.handle_transaksi, name="handle_transaksi"),
    path("submit_transaksi/", views.submit_transaksi, name="submit_transaksi")
]