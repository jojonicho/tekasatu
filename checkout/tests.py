from django.test import LiveServerTestCase, TestCase, Client
from django.apps import apps
from barang.models import Barang, Kategori
from django.contrib.auth.models import User
from keranjang.models import Keranjang, TransaksiBarang
from timauth.forms import UserForm
from django.shortcuts import reverse
import time
import os
from .models import TransaksiCepat
from barang.models import Barang
from keranjang.models import Keranjang
from kupon.models import Kupon

import datetime

from django.contrib.auth.models import User

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Create your tests here.

class TransaksiCepatTestModels(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("testnewuser", password = "passwordcobaan545")
        self.new_tc = TransaksiCepat.objects.create(user = self.new_user)

    def test_transaksicepat_to_str(self):
        self.assertIn("Transaksi cepat milik testnewuser", str(self.new_tc))

class TransaksiCepatTestViews(TestCase):
    def setUp(self):
        self.submit_transaksi_url = reverse("checkout:submit_transaksi")
        self.handle_transaksi_url = reverse("checkout:handle_transaksi")
        self.client = Client()
        self.test_kategori = Kategori.objects.create(
            nama = "test_kategori"
        )

        self.test_barang = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 5000,
            stok_barang = 100,
            deskripsi_barang = "test_deskripsi",
            kategori = self.test_kategori
        )

        self.new_user = User.objects.create_user("testnewuser", password = "passwordcobaan545")
        self.new_keranjang = Keranjang.objects.create(nama_keranjang = self.new_user)
        self.new_tc = TransaksiCepat.objects.create(user = self.new_user)

        # buat TB
        self.new_tb = TransaksiBarang.objects.create(barang = self.test_barang, jumlah = 9)

        # masukkin TB ke keranjang user.
        self.new_keranjang.isi_keranjang.add(self.new_tb)

        self.test_kupon = Kupon.objects.create(
            name = "test_kupon",
            percentage = 50,
            kadaluarsa = datetime.datetime(2050, 5, 17),
            min_price = 10000
        )

    def test_submit_transaksi_shows_total_harga(self):
        self.client.login(username="testnewuser", password="passwordcobaan545")
        response = self.client.get(self.submit_transaksi_url)
        self.assertContains(response, "45000")

    def test_handle_transaksi(self):
        self.client.login(username="testnewuser", password="passwordcobaan545")
        response = self.client.post(self.handle_transaksi_url, {"nama_kupon_dipilih": "test_kupon"}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertJSONEqual(str(response.content, encoding='utf8'), {"status" : "success"})

    def test_barang_tidak_cukup(self):
        self.test_tidak_cukup = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 5000,
            stok_barang = 1,
            deskripsi_barang = "test_deskripsi",
            kategori = self.test_kategori
        )

        self.tb_tidak_cukup = TransaksiBarang.objects.create(barang = self.test_tidak_cukup, jumlah = 10)
        self.new_keranjang.isi_keranjang.add(self.tb_tidak_cukup)

        self.client.login(username="testnewuser", password="passwordcobaan545")
        response = self.client.post(self.handle_transaksi_url, {"nama_kupon_dipilih": "test_kupon"}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEquals(eval(str(response.content, encoding='utf8'))["status"], "failed")
    

        

class CheckOutUnitTest(TestCase):
    def does_checkout_page_exist(self):
        response = Client().get('/transaksicepat/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_redirects_without_login(self):
        response = Client().get('/transaksicepat/submit_transaksi')
        self.assertEquals(301, response.status_code)

class CheckOutFunctionalityTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome(executable_path='./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_checkout_clickable(self):
        selenium = self.browser
        test_kategori = Kategori.objects.create(
            nama = "test_kategori"
        )
        test_kategori.save()

        wait = WebDriverWait(selenium, 5)

        abc = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 100000,
            stok_barang = 100,
            deskripsi_barang = "test_deskripsi",
            kategori = test_kategori
        )
        abc.save()

        user = User.objects.create_user("della1029","della@gmail.com","cape777")
        user.save()
        selenium.get(self.live_server_url + "/login")
        
        login_user_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_login')))
        login_pass_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_password')))
        login_sub_but = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'primaryAction')))
        login_user_table.send_keys("della1029")
        login_pass_table.send_keys("cape777")
        login_sub_but.send_keys(Keys.RETURN)
        time.sleep(5)

        name_link = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'centering')))
        name_link.click()
        time.sleep(5)

        tambah_but = wait.until(EC.element_to_be_clickable((By.ID, 'tambah')))
        tambah_but.click()
        time.sleep(5)

        #keranjang = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'modal_keranjang')))
        #keranjang.click()
        #time.sleep(5)

        #checkout_button = wait.until(EC.element_to_be_clickable((By.ID, 'checkout')))
        #time.sleep(5)

class FunctionalTestKupon(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        
        self.test_kategori = Kategori.objects.create(
            nama = "test_kategori"
        )

        self.test_barang = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 5000,
            stok_barang = 100,
            deskripsi_barang = "test_deskripsi",
            kategori = self.test_kategori
        )

        self.new_user = User.objects.create_user("stevenbukitcemara", password = "passwordcobaan545")
        self.new_keranjang = Keranjang.objects.create(nama_keranjang = self.new_user)
        self.new_tc = TransaksiCepat.objects.create(user = self.new_user)

        # buat TB
        self.new_tb = TransaksiBarang.objects.create(barang = self.test_barang, jumlah = 9)

        # masukkin TB ke keranjang user.
        self.new_keranjang.isi_keranjang.add(self.new_tb)

        self.test_kupon = Kupon.objects.create(
            name = "test_kupon",
            percentage = 50,
            kadaluarsa = datetime.datetime(2050, 5, 17),
            min_price = 10000
        )

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()


    def test_choose_kupon_and_see_history(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)

        selenium.get(self.live_server_url + "/login")

        login_user_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_login')))
        login_pass_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_password')))
        login_sub_but = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'primaryAction')))
        login_user_table.send_keys("stevenbukitcemara")
        login_pass_table.send_keys("passwordcobaan545")
        login_sub_but.send_keys(Keys.RETURN)

        selenium.get(self.live_server_url + "/transaksicepat/submit_transaksi")

        cari_kupon_btn = wait.until(EC.element_to_be_clickable((By.ID, "cari-kupon-btn")))
        cari_kupon_btn.send_keys(Keys.RETURN)

        kupon_search_result = wait.until(EC.element_to_be_clickable((By.ID, "specific-kupon")))

        # Test apakah muncul kupon di search?
        self.assertIn("test_kupon", selenium.page_source)

        kupon_search_result.click()

        # Test apakah kupon memotong harga?
        self.assertIn("22500", selenium.page_source)

        checkout_btn = wait.until(EC.element_to_be_clickable((By.ID, "checkout-btn")))
        checkout_btn.send_keys(Keys.RETURN)
        self.assertIn("Transaksi berhasil", selenium.page_source)

        # Test apakah sudah tercatat dalam history?
        selenium.get(self.live_server_url + "/transaksi/history")

        wait.until(EC.visibility_of_element_located((By.ID, "transaksi-history-card")))
        self.assertIn("test_barang (9)", selenium.page_source)
        self.assertIn("test_kupon", selenium.page_source)

        
