from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from keranjang.models import Keranjang
from .models import TransaksiCepat
from kupon.models import Kupon
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def submit_transaksi(request):
    keranjang = request.user.keranjang.isi_keranjang.all()
    checkout_list = {
        tb : tb.barang.harga_barang * tb.jumlah for tb in keranjang
    }

    total_harga = 0
    for harga in checkout_list.values():
        total_harga += harga

    context = {
        "total_harga" : total_harga,
        "checkout_list" : checkout_list.items(),
        "checkout_page" : True
    }
    # checkout_list = dictionary {TransaksiBarang : harga total barang itu.}

    return render(request, "transaksicepat.html", context)

@csrf_exempt
def handle_transaksi(request):
    if request.method == "POST" and request.is_ajax():
        failed_items = []
        for transaksi_barang in request.user.keranjang.isi_keranjang.all():
            jumlah_dibeli = transaksi_barang.jumlah
            actual_stock = transaksi_barang.barang.stok_barang
            if jumlah_dibeli > actual_stock:
                failed_items.append(transaksi_barang.barang.nama_barang)

        if len(failed_items) != 0:
            return JsonResponse({"status" : "failed", "failed_items" : failed_items})

        nama_kupon = request.POST["nama_kupon_dipilih"]
        kupon_terpilih_instance = None
        if nama_kupon != "None":
            kupon_terpilih_instance = Kupon.objects.get(name = nama_kupon)
        
        new_transaksi_cepat = TransaksiCepat.objects.create(
            user = request.user,
            kupon = kupon_terpilih_instance
        )
        
        # kurangi stok barang yang dibeli dan tambah ke transaksicepat
        for tb in request.user.keranjang.isi_keranjang.all():
            new_transaksi_cepat.barang.add(tb)
            barang_instance = tb.barang
            barang_instance.stok_barang -= tb.jumlah
            barang_instance.save()

        new_transaksi_cepat.save()

        # kosongkan keranjang si user.
        request.user.keranjang.isi_keranjang.clear()

        return JsonResponse({"status" : "success"})