from django.db import models
from django.contrib.auth.models import User
from keranjang.models import Keranjang, TransaksiBarang
from kupon.models import Kupon

# Create your models here.
class TransaksiCepat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    barang = models.ManyToManyField(TransaksiBarang)
    kupon = models.ForeignKey(Kupon, on_delete=models.CASCADE, default= None, blank=True, null=True)
    tanggal_beli = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Transaksi cepat milik {self.user} dengan kupon: {self.kupon} pada {self.tanggal_beli}"