from django.shortcuts import render
from django.http import JsonResponse
from .models import Kupon
from .forms import KuponForm
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt

from datetime import date

@csrf_exempt
def index(request):
    qs = Kupon.objects.all()
    context = {
        'queryset' : qs,
        'form' : KuponForm()
    }

    if request.method == 'POST':
        name = request.POST['name']
        percentage = request.POST['percentage'] if request.POST['percentage'] != '' else 0
        harga_total = request.POST['total_harga']

        # Filter that name contains name, percentage >= searched %, min_price <= minimum harga, kadaluarsa > tanggal hari ini
        qs = qs.filter(name__icontains=name)
        qs = qs.filter(percentage__gte = percentage )
        qs = qs.filter(min_price__lte = harga_total)
        qs = qs.filter(Q(kadaluarsa__gte = date.today()))
        context['queryset'] = qs

        result = {}
        for coupon in qs.all():
            result[str(coupon)] = {
                "percentage" : coupon.percentage,
                "minimum_price" : coupon.min_price
            }

        if request.is_ajax():
            return JsonResponse(result)

        return render(request, 'kupon.html', context)
    return render(request, 'kupon.html', context)

    
    