from django.urls import path, include
from . import views

app_name = 'kupon'

urlpatterns = [
    path('', views.index, name="kupon-index"),
]
