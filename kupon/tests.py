from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Kupon
from django.utils import timezone
import datetime

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("kupon:kupon-index")
        self.test_kupon = Kupon.objects.create(
            name = "test_kupon",
            percentage = 50,
            kadaluarsa = datetime.datetime(2050, 5, 17),
            min_price = 10000
        )

    def test_kupon_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "kupon.html")

    def test_kupon_POST(self):
        response = self.client.post(self.index_url, {
            "name" : "test",
            "percentage" : "",
            "total_harga" : 0
        })
        self.assertEquals(response.status_code, 200)

class TestModels(TestCase):
    def setUp(self):
        self.kupon_test = Kupon.objects.create(
            name = "test_kupon",
            percentage = 50,
            kadaluarsa = timezone.now(),
            min_price = 10000
        )
    
    def test_kupon_name(self):
        self.assertEquals(str(self.kupon_test), "test_kupon")
