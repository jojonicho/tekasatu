from django import forms
from django.forms import ModelForm, Textarea
# from django.utils.timezone import now
from .models import Kupon

class KuponForm(forms.ModelForm):
    class Meta:
        model = Kupon
        fields = ['name', 'percentage']
        # widgets = {
        #     'name': Textarea(attrs={'rows': 1}),
        # }