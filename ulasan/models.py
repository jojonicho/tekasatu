from django.db import models
from barang.models import Barang
from django.contrib.auth.models import User

class Ulasan(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE, default=None)
    ulasan = models.CharField(max_length=1000)
    bintang = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f"Ulasan {self.barang.nama_barang} oleh {self.user.username}"

# Create your models here.
