from django import forms
from .models import *

class UlasanForm(forms.ModelForm):

    class Meta:
        model = Ulasan
        exclude = ["user", "barang"]