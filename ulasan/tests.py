from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from django.contrib.auth.models import User
from barang.models import Barang, Kategori
import time

# Create your tests here.
class UlasanAsyncFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

        self.new_user = User.objects.create_user("stevenbukitcemara", password = "passwordcobaan545")

        self.test_kategori = Kategori.objects.create(
            nama = "test_kategori"
        )

        self.test_barang = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 5000,
            stok_barang = 100,
            deskripsi_barang = "test_deskripsi",
            kategori = self.test_kategori
        )

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_add_ulasan_asynchronously(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)

        selenium.get(self.live_server_url + "/login")

        login_user_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_login')))
        login_pass_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_password')))
        login_sub_but = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'primaryAction')))
        login_user_table.send_keys("stevenbukitcemara")
        login_pass_table.send_keys("passwordcobaan545")
        login_sub_but.send_keys(Keys.RETURN)

        selenium.get(self.live_server_url + "/" + str(self.test_barang.id))

        desc_input = wait.until(EC.element_to_be_clickable((By.ID, 'review-desc')))
        star_input = wait.until(EC.element_to_be_clickable((By.ID, 'review-star')))

        desc_input.send_keys("Nice item!")
        star_input.send_keys("4")

        submit_ulasan_btn = wait.until(EC.element_to_be_clickable((By.ID, 'submit-ulasan-btn')))
        submit_ulasan_btn.send_keys(Keys.RETURN)

        time.sleep(5)
        self.assertIn("Nice item!", selenium.page_source)
