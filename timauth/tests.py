from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from django.test import Client
from django.urls import resolve
from .views import signup
from django.contrib.auth.models import User
from allauth.account.views import LoginView, SignupView

class AuthUnitTest(TestCase):

    def test_auth_signup_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('signup.html')

    def test_auth_signup_post_is_exist(self):
        response = Client().post('/signup/', {'username':'timtam123', 'password1':"321tamtim", 
        'password2':"321tamtim", "email":"timtam@timtam.com", "nama_lengkap":"tim tam"})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('login.html')

    def test_auth_signup_post_is_exist_different_password(self):
        response = Client().post('/signup/', {'username':'bembeng', 'password1':"321bengbem", 'password2':"bengbem123",
        "email":"babam@abba.com","nama_lengkap":"beng beng"})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('signup.html')

    def test_auth_login_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_auth_login_post_success(self):
        response = Client().post('/login/', {'username':'timtam123', 'password':'321tamtim'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('barang.html')
    
    def test_auth_login_post_fail(self):
        response = Client().post('/login/', {'username':'timtam123', 'password':'joni'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login.html')

    def test_auth_signup_function(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)


    def test_auth_model(self):
        User.objects.create(username='joni', password='passwordjoni')
        count = User.objects.all().count()
        self.assertEqual(count, 1)
