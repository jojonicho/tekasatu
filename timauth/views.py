from django.shortcuts import render, reverse, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from .forms import UserForm, UserSigninForm
from barang.views import index

from keranjang.models import Keranjang
from django.dispatch.dispatcher import receiver
from allauth.account.signals import user_signed_up, user_logged_in

@receiver(user_logged_in)
def user_log_(request, user, **kwargs):
    try:
        Keranjang.objects.get(nama_keranjang = user)
    except:
        Keranjang.objects.create(nama_keranjang = user)
    # Keranjang.objects.create(nama_keranjang = user)

def signup(request):
    context = {
        'form': UserSigninForm
    }
    if request.method == "GET":
        return render(request, "signup.html", context=context)
    if request.method == 'POST':
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if password1 != password2:
            context['error'] = "Password do not match!"
            return render(request, "signup.html", context=context)
        else:
            username = request.POST['username']
            email = request.POST['email']
            nama_lengkap = request.POST['nama_lengkap'].split()
            nama_depan = nama_lengkap[0]
            nama_belakang = ""
            if(len(nama_lengkap) > 1):
                nama_belakang = " ".join(nama_lengkap[1:])
            user = User.objects.create_user(username=username, password=password1, email=email,
                                    first_name=nama_depan, last_name=nama_belakang)
            # Buat Keranjang baru untuk setiap orang yang daftar.
            Keranjang.objects.create(nama_keranjang = user)

            return redirect('/login/')
