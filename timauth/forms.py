from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class UserForm(forms.Form):
    username = forms.CharField(max_length=20)
    password = forms.CharField(max_length=20, widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username','password']

class UserSigninForm(UserCreationForm):
    nama_lengkap = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ("nama_lengkap","username","email")