from .views import KuponViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'kupon', KuponViewSet)
urlpatterns = router.urls
