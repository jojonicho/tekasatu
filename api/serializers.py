from rest_framework import serializers
from kupon.models import Kupon

class KuponSerializer(serializers.ModelSerializer):
    # subtotal = serializers.SerializerMethodField()
    
    class Meta:
        model = Kupon
        fields = ['id', 'name', 'percentage', 'kadaluarsa', 'min_price']


        # def create(self, validated_data):
        #     item_validated_data = validated_data.pop('item')
        #     item = Kupon.objects.create(**validated_data)
        #     item_serializer = self.fields['item']
        #     # for each in item_validated_data:
        #     #     each['item'] = item
        #     items = item_serializer.create(item_validated_data)
        #     return item

    # def get_subtotal(self, obj):
    #         return obj.price * obj.quantity