from rest_framework import viewsets, status
# from rest_framework.filters import OrderingFilter
from django_filters import rest_framework
from rest_framework.response import Response
from rest_framework import filters

from kupon.models import Kupon

from .serializers import KuponSerializer

from django.db import models

from django.http import JsonResponse

class KuponViewSet(viewsets.ModelViewSet) :
    search_fields = ['name','percentage']
    filter_backends = (
        rest_framework.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter,)
    filter_fields = ('name', 'percentage')
    serializer_class = KuponSerializer
    queryset = Kupon.objects.all()