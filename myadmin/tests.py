from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.utils import timezone
from kupon.models import Kupon
from barang.models import Barang, Kategori

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("myadmin:index")
        self.kupon_url = reverse("myadmin:kupon")
        self.barang_url = reverse("myadmin:barang")
        self.kategori_url = reverse("myadmin:kategori")

    def test_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "admin.html")

    def test_kupon_GET(self):
        response = self.client.get(self.kupon_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "adminkupon.html")

    def test_barang_GET(self):
        response = self.client.get(self.barang_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "adminbarang.html")

    def test_kategori_GET(self):
        response = self.client.get(self.kategori_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "adminkategori.html")

    def test_kupon_POST(self):
        response = self.client.post(self.kupon_url,{
            "name" : "test_kupon",
            "percentage" : 50,
            "kadaluarsa" : "2021-12-12",
            "min_price" : 50000,
        })
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Kupon.objects.first().name, "test_kupon")
    
    def test_barang_POST(self):
        response = self.client.post(self.kategori_url,{
            "nama" : "kategori_test"
        })
        response = self.client.post(self.barang_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "this is a description",
            "kategori" : 0
        })
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Barang.objects.first().nama_barang, "test_barang")

    def test_kategori_POST(self):
        response = self.client.post(self.kategori_url,{
            "nama" : "kategori_test"
        })
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Kategori.objects.first().nama, "kategori_test")