from django.shortcuts import render
from barang.models import Barang, Kategori
from kupon.models import Kupon
from .forms import KuponForm, BarangForm, KategoriForm
# Create your views here.

# from .forms import AcaraForm
# from acara.models import Acara, Peserta
from django.db.models import Q
# Create your views here.
def index(request):
    return render(request, "admin.html")

def kupon(request):
    qs = Kupon.objects.all()
    context = {
        'queryset' : qs,
        'form' : KuponForm()
    }
    if request.method == 'POST':
        name = request.POST['name']
        percentage = request.POST['percentage']
        kadaluarsa = request.POST['kadaluarsa']
        min_price = request.POST['min_price']
        Kupon.objects.create(name=name, percentage=percentage, kadaluarsa=kadaluarsa, min_price=min_price)
        return render(request, "adminkupon.html", context)
    return render(request, "adminkupon.html", context)

def barang(request):
    qs = Barang.objects.all()
    context = {
        'queryset' : qs,
        'form' : BarangForm()
    }
    if request.method == 'POST':
        name = request.POST['nama_barang']
        price = request.POST['harga_barang']
        stock = request.POST['stok_barang']
        desc = request.POST['deskripsi_barang']
        Barang.objects.create(nama_barang=name, harga_barang=price, stok_barang=stock, deskripsi_barang=desc)
        return render(request, "adminbarang.html", context)
    return render(request, "adminbarang.html", context)

def kategori(request):
    qs = Kategori.objects.all()
    context = {
        'queryset' : qs,
        'form' : KategoriForm()
    }
    if request.method == 'POST':
        name = request.POST['nama']
        Kategori.objects.create(nama=name)
        return render(request, "adminkategori.html", context)
    return render(request, "adminkategori.html", context)