from django.urls import path, include
from . import views

app_name = 'myadmin'

urlpatterns = [
    path('', views.index, name="index"),
    path('kupon/', views.kupon, name="kupon"),
    path('barang/', views.barang, name="barang"),
    path('kategori/', views.kategori, name="kategori"),
]
