from django import forms
from kupon.models import Kupon
from barang.models import Barang, Kategori

class KuponForm(forms.ModelForm):
    class Meta:
        model = Kupon
        fields = ['name', 'percentage', 'kadaluarsa', 'min_price']

class BarangForm(forms.ModelForm):
    class Meta:
        model = Barang
        fields = ['nama_barang', 'harga_barang', 'stok_barang', 'deskripsi_barang','kategori']

class KategoriForm(forms.ModelForm):
    class Meta:
        model = Kategori
        fields = ['nama']