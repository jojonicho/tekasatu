# Tugas Kelompok 1 PPW-J

[![Pipeline Status](https://gitlab.com/jojonicho181/tekasatu/badges/master/pipeline.svg)](https://gitlab.com/jojonicho181/tekasatu/pipelines)

[![coverage report](https://gitlab.com/jojonicho181/tekasatu/badges/master/coverage.svg)](https://gitlab.com/jojonicho181/tekasatu/-/commits/master)

Anggota Kelompok:
- Della Patricia Siregar
- Jonathan Nicholas
- Salman Marsha
- Steven

Link Herokuapp : http://tk1-ppw.herokuapp.com/