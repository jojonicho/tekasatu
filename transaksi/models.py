from django.db import models
from barang.models import Barang
from kupon.models import Kupon

class Transaksi(models.Model):
    @property
    def get_transaksi(self):
        try:
            return self.barang.harga_barang * (100 - self.kupon.percentage) // 100
        except:
            return self.barang.harga_barang # Biar kuponnya opsional

    nama_pembeli = models.TextField(max_length=50)
    barang = models.ForeignKey(Barang,on_delete=models.PROTECT)
    total_transaksi = get_transaksi
    kupon = models.ForeignKey(Kupon,on_delete=models.PROTECT,null=True, blank=True) # Biar kuponnya opsional
    tanggal_beli = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {} @ {}".format(self.nama_pembeli, self.barang, self.tanggal_beli)

    
        

    