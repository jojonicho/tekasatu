from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Transaksi
from django.utils import timezone
from barang.models import Barang, Kategori
from kupon.models import Kupon
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model, login

class TestModels(TestCase):
    def setUp(self):
        self.kategori_test = Kategori.objects.create(
            nama = "kategori_test"
        )
        self.barang_test = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 50000,
            stok_barang = 10,
            deskripsi_barang = "deskripsi_barang_test",
            kategori = self.kategori_test
        )
        self.kupon_test = Kupon.objects.create(
            name = "test_kupon",
            percentage = 50,
            kadaluarsa = "2099-12-12",
            min_price = 10000
        )
        self.transaksi_test = Transaksi.objects.create(
            nama_pembeli = "Pembeli Test",
            barang = self.barang_test,
            kupon = self.kupon_test,
            tanggal_beli = timezone.now()
        )  

    def test_str_transaksi(self):
        self.assertTrue("Pembeli Test - test_barang" in str(self.transaksi_test))

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.transaksi_url = reverse("transaksi:history")
        self.checkout_url = reverse("transaksi:checkout")
        self.add_transaksi_url = reverse("transaksi:add_transaksi")
        self.signup = reverse("timauth:signup")
        self.login = reverse("account_login")
        self.kategori_test = Kategori.objects.create(
            nama = "kategori_test"
        )
        self.barang_test = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 50000,
            stok_barang = 10,
            deskripsi_barang = "deskripsi_barang_test",
            kategori = self.kategori_test
        )
        self.kupon_test = Kupon.objects.create(
            name = "test_kupon",
            percentage = 50,
            kadaluarsa = "2099-12-12",
            min_price = 10000
        )
        self.credentials = {
            'username':'timtam123', 
            'password':"321tamtim"
        }
    def test_login(self):
        response = self.client.post('/signup/', {'username':'timtam123', 'password1':"321tamtim", 
        'password2':"321tamtim", "email":"timtam@timtam.com", "nama_lengkap":"tim tam"})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('login.html')
        login = self.client.post('/login/', {'username':'timtam123', 'password':'321tamtim'})
        self.assertEqual(login.status_code, 200)
        self.assertTemplateUsed('barang.html')
        return login
    
    def test_history_GET_redirects_without_login(self):
        response = self.client.get(self.transaksi_url)
        self.assertEquals(response.status_code, 302)

    def test_checkout_GET(self):
        response = self.client.get(self.checkout_url)
        self.assertEquals(response.status_code, 302)


    def test_checkout_POST_without_kupon(self):
        response = self.client.post(self.checkout_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
        })
        self.assertEquals(response.status_code, 302)

    def test_checkout_POST_without_kupon_after_login(self):
        self.test_login()
        response = self.client.post(self.checkout_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
        })
        self.assertEquals(response.status_code, 302)
        # self.assertContains(response, "test_barang")

    def test_checkout_POST_with_kupon(self):
        response = self.client.post(self.checkout_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
            "nama_kupon" : "test_kupon",
            "persentase_kupon" : 10
        })
        self.assertEquals(response.status_code, 302)

    def test_checkout_POST_with_kupon_after_login(self):
        self.test_login()
        response = self.client.post(self.checkout_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
            "nama_kupon" : "test_kupon",
            "persentase_kupon" : 10
        })
        self.assertEquals(response.status_code, 302)
        # self.assertTemplateUsed(response, "checkout.html")
        # self.assertContains(response, "test_kupon")
    
    def test_checkout_POST_choose_kupon(self):
        response = self.client.post(self.checkout_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
            "kupon_terpilih" : "test_kupon---50"
        })
        self.assertEquals(response.status_code, 302)
    
    def test_checkout_POST_choose_kupon(self):
        response = self.test_login()
        response = self.client.post(self.checkout_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
            "kupon_terpilih" : "test_kupon---50"
        })
        self.assertEquals(response.status_code, 302)
        # self.assertTemplateUsed(response, "checkout.html")
        # Test harga benar atau tidak.
        # self.assertContains(response, "Rp. {}".format(int(self.barang_test.harga_barang * (100-float(self.kupon_test.percentage)) / 100)))

    def test_add_transaksi_POST_with_kupon(self):
        oldStockCount = self.barang_test.stok_barang
        response = self.client.post(self.add_transaksi_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
            "kupon_terpilih_nama" : "test_kupon",
            "kupon_terpilih_persentase" : 50,
            "nama_depan" : "Pembeli",
            "nama_belakang" : "Test"
        })
        self.assertEquals(Transaksi.objects.first().nama_pembeli, "Pembeli Test")
        self.assertEquals(Transaksi.objects.first().total_transaksi, 25000)
        self.assertEquals(self.barang_test.stok_barang, oldStockCount)

    def test_add_transaksi_POST_without_kupon(self):
        oldStockCount = self.barang_test.stok_barang
        response = self.client.post(self.add_transaksi_url,{
            "nama_barang" : "test_barang",
            "harga_barang" : 50000,
            "stok_barang" : 10,
            "deskripsi_barang" : "deskripsi_barang_test",
            "nama_depan" : "Pembeli",
            "nama_belakang" : "Test"
        })
        self.assertEquals(Transaksi.objects.first().nama_pembeli, "Pembeli Test")
        self.assertEquals(self.barang_test.stok_barang, oldStockCount)
        self.assertEquals(Transaksi.objects.first().total_transaksi, 50000)