from django.urls import path
from . import views

app_name = "transaksi"

urlpatterns = [
    path("history/",views.history_view, name="history"),
    path("checkout/",views.checkout, name="checkout"),
    path("add_transaksi/",views.add_transaksi, name="add_transaksi")
]