from django.shortcuts import render, redirect
from .models import Transaksi
from barang.models import Barang
from kupon.models import Kupon
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from checkout.models import TransaksiCepat

def get_total_harga_transaksi(transaksi_cepat):
    total_harga = 0
    for transaksi_barang in transaksi_cepat.barang.all():
        total_harga += transaksi_barang.barang.harga_barang * transaksi_barang.jumlah
    
    if transaksi_cepat.kupon != None:
        return int(total_harga - (total_harga*(100-transaksi_cepat.kupon.percentage)/100))
    else:
        return total_harga

@login_required(login_url='/login/')
def history_view(request):
    transaksi_list = [
        (transaksi_cepat, get_total_harga_transaksi(transaksi_cepat), ", ".join(map(str,transaksi_cepat.barang.all()))) for transaksi_cepat in TransaksiCepat.objects.filter(user = request.user).order_by("-tanggal_beli").all()
    ]
    context = {
        "transaksi_list" : transaksi_list
    }
    return render(request, "history.html", context)

@login_required(login_url='/login/')
def checkout(request):
    if request.method == "POST":
        # Detail barang yang di checkout
        nama_barang = request.POST["nama_barang"]
        harga_barang = request.POST["harga_barang"]
        stok_barang = request.POST["stok_barang"]
        deskripsi_barang = request.POST["deskripsi_barang"]
        
        # Pencarian kupon
        try:
            nama_kupon = request.POST["nama_kupon"] if request.POST["nama_kupon"] else ""
            persentase_kupon = request.POST["persentase_kupon"] if request.POST["persentase_kupon"] else 0
            kupon_list = Kupon.objects.filter(name__icontains = nama_kupon)
            kupon_list = kupon_list.filter(percentage__gte = persentase_kupon)
            kupon_list = kupon_list.filter(kadaluarsa__gte = timezone.now(), min_price__lte = harga_barang) # Check kadaluarsa
        except:
            kupon_list = Kupon.objects.filter(kadaluarsa__gte = timezone.now(), min_price__lte = harga_barang)[:10]
            nama_kupon = ""
            persentase_kupon = ""

        barang_dibeli = Barang.objects.get(
            nama_barang = nama_barang,
            harga_barang = harga_barang,
            stok_barang = stok_barang,
            deskripsi_barang = deskripsi_barang
        )

        # Milih kupon
        try:
            kupon_terpilih = request.POST["kupon_terpilih"].split("---")
            nama_kupon_terpilih = kupon_terpilih[0]
            persentase_kupon_terpilih = kupon_terpilih[1]
            harga_setelah_potongan = int(barang_dibeli.harga_barang * (100-float(persentase_kupon_terpilih)) / 100)
        except:
            nama_kupon_terpilih = ""
            persentase_kupon_terpilih = ""
            harga_setelah_potongan = None

        context = {
            "barang_dibeli" : barang_dibeli,
            "kupon_list" : kupon_list,
            "nama_kupon" : nama_kupon,
            "persentase_kupon" : persentase_kupon,
            "nama_kupon_terpilih" : nama_kupon_terpilih,
            "persentase_kupon_terpilih" : persentase_kupon_terpilih,
            "harga_setelah_potongan" : harga_setelah_potongan
        }

        return render(request, "checkout.html", context)
    else:
        return redirect("barang:index")

def add_transaksi(request):
    if request.method == "POST":
        # Detail barang yang di checkout
        nama_barang = request.POST["nama_barang"]
        harga_barang = request.POST["harga_barang"]
        stok_barang = request.POST["stok_barang"]
        deskripsi_barang = request.POST["deskripsi_barang"]

        # Nama pembeli
        nama_pembeli = request.POST["nama_depan"] + " " + request.POST["nama_belakang"]

        # Barang yang dibeli
        barang_dibeli = Barang.objects.get(
            nama_barang = nama_barang,
            harga_barang = harga_barang,
            stok_barang = stok_barang,
            deskripsi_barang = deskripsi_barang
        )

        # Kupon yang dipilih dan di submit di hidden form
        try:
            nama_kupon_terpilih = request.POST["kupon_terpilih_nama"]
            persentase_kupon_terpilih = request.POST["kupon_terpilih_persentase"]
            kupon_terpilih = Kupon.objects.get(name=nama_kupon_terpilih, percentage = persentase_kupon_terpilih)
        except:
            kupon_terpilih = None

        # Create transaksi
        new_transaksi = Transaksi.objects.create(
            nama_pembeli = nama_pembeli,
            barang = barang_dibeli,
            kupon = kupon_terpilih
        ).save()
        
        barang_dibeli.stok_barang -= 1 # Kurangi jumlah stok 1
        barang_dibeli.save()

    return redirect("barang:index")