function detail() {
    $(".modal-body").empty()
    $(".untuk-harga").empty()
    $(".transaksi-cepat").empty()

    $.ajax({
        url : $("meta[name='untukdetail']").attr("content"),
        type : "GET",
        success : function(res) {
            console.log(res.data)
            var tempo = res.data
            var total_price = 0;
            for (i in tempo) {
                total_price +=  parseInt(tempo[i].harga) * parseInt(tempo[i].stok)
                var temp = "<div class='d-flex flex column justify-content-between'>"
                console.log(parseInt(tempo[i].harga) * parseInt(tempo[i].stok))
                temp += "<div><h3>" + tempo[i].nama + "</h3><h6>" + tempo[i].stok+ "</h6>"+
                    "<h3> Harga = " + parseInt(tempo[i].harga) * parseInt(tempo[i].stok) + "</h3></div>"
                    + "<div class='tombol'><button class='kurang btn btn-primary' id='"+ tempo[i].ids +"''>-</button><button class='tambah btn btn-danger' id='"+ tempo[i].ids +"''>+</button></div>"
                $(".modal-body").append(temp);
            }
            $(".modal-body").append("</div>");
            $(".untuk-harga").append("Total : " + total_price);

        }
    })
}



$("#tambah").click(function(){
    var holder = $(".nama_barang").attr("id")
    // alert(holder)
    $.post($("meta[name='untukcreate']").attr("content"), {
        namabarang : holder,
    }).done(() => {
        alert("Barang berhasil ditambahkan ke keranjang")
    })
})


$(".modal_keranjang").click(function(){
    // alert("anjay")
    detail()

})

$(".modal-body").click((e)=>{
    if ($(e.target).hasClass("kurang")) {
        $.ajax({
            url : $("meta[name='untukkurang']").attr("content"),
            type : "POST",
            data : {
                id_target : $(e.target).attr("id")
            },
            success : function() {
                detail()
                alert("Barang dikurangi")
            },
        })
    }

    else if ($(e.target).hasClass("tambah")) {
        $.ajax({
            url : $("meta[name='untukadd']").attr("content"),
            type : "POST",
            data : {
                id_target : $(e.target).attr("id")
            },
            success : function() {
                detail()
                alert("Item ditambahkan")
            }
        })
    }
})

$("#checkout").click(function() {
    $.ajax({
        url: $("meta[name='untukdetail']").attr("content"),
        type: "GET",
        success: function(res) {
            var bool = true;
            var tempo = res.data
            var baranglebih = "";
            for (i in tempo) {
                if (tempo[i].stok > tempo[i].stock_asli) {
                    baranglebih += tempo[i].nama
                    bool = false;
                }
            }
            if (!bool) {
                alert('Stok kurang! Kurangi ' + baranglebih);
            } else if ($(".checkout-ref").attr("href") == "/transaksicepat/"){
                alert("Transaksi berhasil!")
            } else {
                $(".checkout-ref").attr("href", "/transaksicepat/");
                alert('Transaksi sedang di proses! Click kembali untuk checkout!'); 
            } 
        },
        error: function() {
            alert('ERROR OCCURED');
        }
    })
})
