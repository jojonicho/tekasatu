$(document).ready(function(e) {
    $.ajax({
        url: $("meta[name='untukdetail']").attr("content"),
        type: "GET",
        success: function(res) {
            console.log(res.data);
            var tempo = res.data
            var total_price = 0;
            for (i in tempo) {
                var temp_price = parseInt(tempo[i].harga) * parseInt(tempo[i].stok);
                total_price += temp_price;
                var temp = "<div class='bg-main3 w-50 p-3 border-history my-2'>";
                temp += "<h3>" + tempo[i].nama + " (" + tempo[i].stok + ") </h3>";
                temp += '<h3> Rp ' + temp_price + '<h3>'
                temp += "</div>";
                $(".transaksi").append(temp);
            }
            var total = "<h3> Total Harga : " + total_price + "</h3>";
            $('.transaksi').append(total);
        },
        error: function() {
            alert('ERROR');
        }
    })
});