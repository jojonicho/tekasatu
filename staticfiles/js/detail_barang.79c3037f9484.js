$(document).ready(function(e) {
    $("#submit-ulasan-btn").click(() => {
        $(".alert").css({"display":"none"})

        var ulasan_text = $("#review-desc").val()
        var star = Number($("#review-star").val())
        var username = $("#submit-ulasan-btn").attr("current_user")
        var id = $("#submit-ulasan-btn").attr("to_id")

        if (star > 5 || star < 1) {
            $("#alert-danger-ulasan").css({"display":"block"})
            return
        }

        $.post( "" , data = {
            ulasan : ulasan_text,
            bintang : star
        }).done( (res) => {
            if (res["status"] == "success") {
                $("#ulasan-container").prepend(
                    `
                        <div class="card mb-1 mt-1 p-3">
                            <h3>${username}</h3>
                            <p>${star}/5 Stars</p>
                            <p>${ulasan_text}</p>
                        </div>
                    `
                )
                $("#alert-success-ulasan").css({"display":"block"})

                $("#review-desc").val("")
                $("#review-star").val("")
            }
        })
    })
});