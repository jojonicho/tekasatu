from django.contrib import admin
from .models import Barang, Kategori
# Register your models here.
admin.site.register(Barang)
admin.site.register(Kategori)