from django.db import models

# Create your models here.
class Kategori(models.Model):
    nama = models.CharField(max_length=15)

    class Meta:
        ordering = ('nama',)
        verbose_name_plural = 'Kategoris'

    def __str__(self):
        return self.nama
    objects = models.Manager()

class Barang(models.Model):
    nama_barang = models.CharField(max_length=50)
    harga_barang = models.IntegerField()
    stok_barang = models.PositiveSmallIntegerField(default=0)
    deskripsi_barang = models.TextField(max_length=100)
    kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return f'{self.nama_barang}'