from django import forms
# from django.utils.timezone import now
from .models import Barang

class SearchForm(forms.ModelForm):
    class Meta:
        model = Barang
        fields = ['nama_barang','kategori']
        labels ={'nama_barang': 'Cari Barang' }