from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Barang, Kategori
from ulasan.models import Ulasan
from django.contrib.auth.models import User

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("barang:index")
        self.test_kategori = Kategori.objects.create(
            nama = "test_kategori"
        )
        self.test_barang = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 100000,
            stok_barang = 100,
            deskripsi_barang = "test_deskripsi",
            kategori = self.test_kategori
        )
        self.test_barang_detail_url = reverse("barang:detail",args=[self.test_barang.id])
        self.test_ulasan = Ulasan.objects.create(
            barang = self.test_barang,
            user = User.objects.create_user("usersatu", password="passwordtest222"),
            ulasan = "ini adalah ulasan",
            bintang = 3
        )

    def test_barang_list_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "barang.html")

    def test_barang_detail_GET(self):
        response = self.client.get(self.test_barang_detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "detail.html")
        self.assertContains(response, "test_deskripsi")

    def test_barang_list_POST(self):
        response = self.client.post(self.index_url, {
            "nama_barang" : "test_barang"
        })
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "barang.html")
        self.assertContains(response, "test_barang")

    def test_barang_detail_POST(self):
        response = self.client.post(self.test_barang_detail_url, {
            "barang" : self.test_barang,
            "nama_pengulas" : "test_pengulas",
            "ulasan" : "ini adalah ulasan",
            "bintang" : 3
        })
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "ini adalah ulasan")
