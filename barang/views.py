from django.shortcuts import render
from .models import Barang
from .forms import SearchForm
from django.db.models import Q
from django.http import JsonResponse
from ulasan.forms import UlasanForm
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt

def index(request):
    semua_barang = Barang.objects.all()
    context={
        'semua_barang':semua_barang,
        # 'user':user,
        'form': SearchForm()
    }
    if request.method == 'POST':
        query = request.POST['nama_barang']
        semua_barang_nama = semua_barang.filter(nama_barang__icontains=query)
        semua_barang_kategori = semua_barang.filter(kategori__nama__icontains=query)
        context['semua_barang'] = semua_barang_nama | semua_barang_kategori
        return render(request, 'barang.html', context)
    return render(request, "barang.html",context)

@csrf_exempt
def detail(request, index):
    detail_barang = Barang.objects.get(pk=index)

    # Ulasan ke barang.
    if request.method == "POST" and request.is_ajax():
        ulasan_form = UlasanForm(request.POST)
        if ulasan_form.is_valid():
            ulasan = ulasan_form.save(commit=False)
            ulasan.barang = detail_barang
            ulasan.user = request.user
            ulasan.save()
            return JsonResponse({"status" : "success"})
        return JsonResponse({"status" : "failed"})

    return render(request, "detail.html", {"detail_barang" : detail_barang, "ulasan_query" : detail_barang.ulasan_set.order_by("-id")})