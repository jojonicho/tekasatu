# Generated by Django 3.0.3 on 2020-03-03 05:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('barang', '0002_auto_20200301_1749'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kategori',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=15)),
            ],
            options={
                'verbose_name_plural': 'Kategoris',
                'ordering': ('nama',),
            },
        ),
    ]
