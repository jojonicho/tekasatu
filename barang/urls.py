from django.urls import path, include
from . import views

app_name = 'barang'
urlpatterns = [
    path('', views.index, name="index"),
    path('<int:index>/', views.detail, name="detail")
]
