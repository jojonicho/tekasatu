$(document).ready(function(e) {
    var total_harga = Number($("#total-harga").text())

    $("#checkout-btn").click(() => {
        var total_harga = $("#new-harga-amount").text() ? Number($("#new-harga-amount").text()) : Number($("#total-harga").text())
        $("#notification-sedang-proses").css({"display":"block"})

        $.post("/transaksicepat/handle_transaksi/", data = {
            "nama_kupon_dipilih" : $(".chosen-kupon").find("#specific-nama-kupon").text() ? $(".chosen-kupon").find("#specific-nama-kupon").text() : "None",
        }).done((res) => {
            $(".alert").css({
                "display":"none"
            })

            if (res["status"] == "success") {
                $(".alert-success").css({
                    "display":"block"
                })

                $("#checkout-btn").attr("disabled",true)

            } else if (res["status"] == "failed") {
                $(".alert-danger").css({
                    "display":"block"
                })

                $("#nama_barang_gagal").text(
                    res["failed_items"].join(",")
                )
            }
        })

    })

    $(".coupon-list").click((e) => {
        if ($(e.target).attr("id") == "specific-kupon") {

            $(".coupon-list").children().removeClass("chosen-kupon")
            
            $(e.target).addClass("chosen-kupon")

            $("#nama-kupon-transaksi").text($(e.target).find("#specific-nama-kupon").text())

            let kupon_percentage = Number($(e.target).find("#specific-percentage-kupon").text())

            $("#total-harga").css({
                "text-decoration" : "line-through"
            })

            $("#new-harga").text("Harga setelah kupon: ")
            $("#new-harga").append(`<span id="new-harga-amount">${String(total_harga - (total_harga * kupon_percentage / 100))}</span>`)
        }
    })

    $("#cari-kupon-btn").click(() => {
        var coupon_name = $("#cari-kupon-name").val()
        var coupon_percentage = Number($("#cari-kupon-percentage").val())
        var total_harga = Number($("#total-harga").text())
        
        $(".coupon-list").empty()
        $(".coupon-list").append(
            "<h4>Searching...</h4>"
        )

        $.post(
            "/kupon/",
            data = {
                name : coupon_name,
                percentage : coupon_percentage,
                total_harga : total_harga
            }
        ).done((res) => {
            $(".coupon-list").empty()
            for (let nama_kupon in res) {
                $(".coupon-list").append(
                    `<div id="specific-kupon" class="m-2 p-3 bg-warning rounded-lg">
                        <span id="specific-nama-kupon">${nama_kupon}</span> (<span id="specific-percentage-kupon">${res[nama_kupon]["percentage"]}</span>% off untuk min. pembelian Rp. ${res[nama_kupon]["minimum_price"]},-)
                    </div>`
                )
            }

            if (jQuery.isEmptyObject(res)) {
                $(".coupon-list").append(
                    "<p>Kupon dengan kriteria tersebut tidak ditemukan :(</p>"
                )
            }

            
        }
        )
    })

    /*
    $.ajax({
        url: $("meta[name='untukdetail']").attr("content"),
        type: "GET",
        success: function(res) {
            console.log(res.data);
            var tempo = res.data
            var total_price = 0;
            for (i in tempo) {
                var temp_price = parseInt(tempo[i].harga) * parseInt(tempo[i].stok);
                total_price += temp_price;
                var temp = "<div class='bg-main3 w-50 p-3 border-history my-2'>";
                temp += "<h3>" + tempo[i].nama + " (" + tempo[i].stok + ") </h3>";
                temp += '<h3> Rp ' + temp_price + '<h3>'
                temp += "</div>";
                $(".transaksi").append(temp);
            }
            var total = "<h3> Total Harga : " + total_price + "</h3>";
            $('.transaksi').append(total);
        },
        error: function() {
            alert('ERROR');
        }
    })
    */
});