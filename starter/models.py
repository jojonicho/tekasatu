from django.db import models

class TulisanModel(models.Model):
    judul = models.TextField(max_length=100)

    def __str__(self):
        return "{} - {}".format(self.judul,self.id)