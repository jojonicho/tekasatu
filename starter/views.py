from django.shortcuts import render
from .models import TulisanModel

def index(request):
    tulisan = TulisanModel.objects.all()
    context = {
        "tulisan" : tulisan
    }
    return render(request,"starter/index.html",context)