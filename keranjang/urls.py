from django.urls import path, include
from . import views

app_name = 'keranjang'
urlpatterns = [
    path('', views.detail, name="detail"),
    path('create/',views.create, name="create"),
    path('reduced/',views.reduced, name="reduced"),
    path('add/',views.add, name="add"),
]
