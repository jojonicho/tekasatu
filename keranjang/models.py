from django.db import models
from django.contrib.auth.models import User
from barang.models import Barang

# Create your models here.
class TransaksiBarang(models.Model):
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    jumlah = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.barang.nama_barang} ({self.jumlah})"

class Keranjang(models.Model):
    nama_keranjang = models.OneToOneField(User, on_delete=models.CASCADE)
    isi_keranjang = models.ManyToManyField(TransaksiBarang)

    def __str__(self):
        return "Keranjang milik {}".format(self.nama_keranjang.username)