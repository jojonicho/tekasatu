from django.test import LiveServerTestCase, TestCase
from django.apps import apps
from barang.models import Barang, Kategori
from django.contrib.auth.models import User
from .apps import KeranjangConfig
from .models import Keranjang, TransaksiBarang
from timauth.forms import UserForm
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Create your tests here.

class AppsConf(TestCase):
    def test_app(self):
        self.assertEqual(KeranjangConfig.name, 'keranjang')
        self.assertEqual(apps.get_app_config('keranjang').name, 'keranjang')

class FuncTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome(executable_path='./chromedriver',chrome_options = chrome_options)
    
    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_barang_ada_di_keranjang_setelah_dimasukkan(self):
        selenium = self.browser
        test_kategori = Kategori.objects.create(
            nama = "test_kategori"
        )
        test_kategori.save()

        wait = WebDriverWait(selenium, 5)

        abc = Barang.objects.create(
            nama_barang = "test_barang",
            harga_barang = 100000,
            stok_barang = 100,
            deskripsi_barang = "test_deskripsi",
            kategori = test_kategori
        )
        abc.save()

        user = User.objects.create_user("salman233","salman@gmail.com","hidupku123")
        user.save()
        selenium.get(self.live_server_url + "/login")
        
        login_user_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_login')))
        login_pass_table = wait.until(EC.element_to_be_clickable((By.ID, 'id_password')))
        login_sub_but = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'primaryAction')))
        login_user_table.send_keys("salman233")
        login_pass_table.send_keys("hidupku123")
        login_sub_but.send_keys(Keys.RETURN)

        name_link = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'centering')))
        name_link.click()

        tambah_but = wait.until(EC.element_to_be_clickable((By.ID, 'tambah')))
        tambah_but.send_keys(Keys.RETURN)

        self.assertIn("test_barang",selenium.page_source)
