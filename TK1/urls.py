from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import  static
from . import settings
from rest_framework.documentation import include_docs_urls
from allauth.account.views import LoginView, SignupView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    #experiment using allauth
    path('socilalaccounts/', include('allauth.urls')),
    path("login/", LoginView.as_view(), name="llogin"),
    path('docs/', include_docs_urls(title='shop_API')),
    path('',include("barang.urls"), name="barang"),
    path('kupon/', include('kupon.urls'), name='kupon'),
    path('api/', include('api.urls'), name='api'),
    path("ulasan/",include("ulasan.urls"), name="ulasan"),
    path("transaksi/",include("transaksi.urls"), name="transaksi"),
    path("myadmin/", include("myadmin.urls") , name="myadmin"),
    path("", include("timauth.urls") , name="timauth"),
    path("keranjang/",include("keranjang.urls"),name="keranjang"),
    path("transaksicepat/", include("checkout.urls"),name="transaksicepat")
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
